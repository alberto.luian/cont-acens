import styled from 'styled-components';

export const Button1 = styled.button`
  cursor: pointer;
  width: 160px;
  height: 35px;
  background: #00ffff;
  color: #8a2be2;
  font-size: 0.8em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid #8a2be2;
  border-radius: 3px;
`;
export const Button2 = styled.button`
  cursor: pointer;
  width: 160px;
  height: 35px;
  background: #00ffff;
  color: #a41111;
  font-size: 0.8em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid #a41111;
  border-radius: 3px;
`;
export const Button3 = styled.button`
  cursor: pointer;
  width: 160px;
  height: 35px;
  background: red;
  color: white;
  font-size: 0.8em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid red;
  border-radius: 3px;
`;
export const Div1 = styled.div`
  height: 100vh;
  width: 100vw;
  background: #00ffff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
export const Div2 = styled.div`
  margin-top: -13px;
  padding: 0px;
`;
