import { useState } from 'react';
import acensicon from './acensicon.png';
import './App.css';
import { Button1, Button2, Button3, Div1, Div2 } from './styles.js';

// eslint-disable-next-line func-names
const App = function () {
  const [count, setCount] = useState(0);
  async function somar() {
    setCount(count + 1);
  }
  async function subtrair() {
    setCount(count - 1);
  }
  async function resetar() {
    setCount(0);
  }
  const styleObj1 = {
    fontSize: 25,
    padding: '0px',
    fontWeight: 650,
    marginTop: '0px',
  };
  const styleObj2 = {
    fontSize: 30,
    padding: '0px',
    fontWeight: 650,
  };
  return (
    // eslint-disable-next-line react/jsx-filename-extension
    <Div1>
      <Div2>
        <img src={acensicon} alt="logo" />
      </Div2>
      <Div2>
        <p style={styleObj2}>ContAcens, o contador da Acens!</p>
      </Div2>
      <Div2>
        <p style={styleObj1}>
          Valor: <text> {count} </text>
        </p>
      </Div2>
      <Div2>
        <Button1 className="click" onClick={somar}>
          Adicionar
        </Button1>
      </Div2>
      <Div2>
        <Button2 className="click" onClick={subtrair}>
          Remover
        </Button2>
      </Div2>
      <Div2>
        <Button3 className="click" onClick={resetar}>
          Resetar
        </Button3>
      </Div2>
    </Div1>
  );
};

export default App;
